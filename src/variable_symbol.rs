use crate::types::Type;

#[derive(Debug, Clone)]
pub struct VariableSymbol {
    pub name: String,
    pub symbol_type: Type,
}

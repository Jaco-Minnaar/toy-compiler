use crate::{
    diagnostic::DiagnosticReport,
    syntax::{lexer::Lexer, tokens::SyntaxTokenContent},
    text::source_text::SourceText,
};

use super::{compilation_unit::CompilationUnit, parser::Parser, tokens::SyntaxToken};

#[derive(Clone)]
pub struct SyntaxTree {
    pub diagnostics: DiagnosticReport,
    pub root: CompilationUnit,
    pub source_text: SourceText,
}

impl SyntaxTree {
    fn new(source_text: SourceText) -> SyntaxTree {
        let mut parser = Parser::new(&source_text);
        let root = parser.parse_compilation_unit();

        let mut diags = DiagnosticReport::new();

        diags.append_report(&parser.diagnostics.borrow());
        SyntaxTree {
            diagnostics: diags,
            root,
            source_text,
        }
    }

    pub fn parse_str(text: &str) -> SyntaxTree {
        let source_text = SourceText::from_str(text);

        let syntax_tree = Self::parse_source_text(source_text);

        syntax_tree
    }

    pub fn parse_source_text(source_text: SourceText) -> SyntaxTree {
        Self::new(source_text)
    }

    pub fn parse_tokens(text: &SourceText) -> Vec<SyntaxToken> {
        let mut lexer = Lexer::new(text.clone());
        let mut tokens = vec![];
        loop {
            let token = lexer.lex();
            if let SyntaxTokenContent::EOF = token.token {
                break;
            } else {
                tokens.push(token.clone());
            }
        }

        return tokens;
    }
}

use crate::{
    diagnostic::{DiagnosticReport, TextSpan},
    syntax::tokens::{BinaryToken, Keyword},
    text::source_text::SourceText,
    types::{IntegerType, Type},
};

use super::tokens::{BinaryTokenKind, Parenthesis, SyntaxToken, SyntaxTokenContent};

pub struct Lexer {
    chars: Vec<char>,
    position: usize,
    pub diagnostics: DiagnosticReport,
}

impl Lexer {
    pub fn new(text: SourceText) -> Lexer {
        let chars = text.chars().collect();

        Lexer {
            chars,
            position: 0,
            diagnostics: DiagnosticReport::new(),
        }
    }

    pub fn lex(&mut self) -> SyntaxToken {
        match self.current_char() {
            Some(c) => {
                let token = match c {
                    '+' => self.read_simple_binary_token(BinaryTokenKind::Plus),
                    '-' => self.read_simple_binary_token(BinaryTokenKind::Minus),
                    '*' => self.read_simple_binary_token(BinaryTokenKind::Star),
                    '/' => self.read_simple_binary_token(BinaryTokenKind::Slash),
                    '(' => self.read_bracket_token(Parenthesis::Open),
                    ')' => self.read_bracket_token(Parenthesis::Close),
                    '{' => self.read_brace_token(Parenthesis::Open),
                    '}' => self.read_brace_token(Parenthesis::Close),
                    '&' => self.read_ampersand_token(),
                    '|' => self.read_pipe_token(),
                    '=' => self.read_equals_token(),
                    '!' => self.read_bang_token(),
                    'A'..='Z' | 'a'..='z' => self.read_letter_token(),
                    '1'..='9' => self.read_number_token(),
                    '\n' | '\r' | '\t' | ' ' => self.read_whitespace_token(),
                    ch if ch.is_whitespace() => self.read_whitespace_token(),
                    ch if ch.is_alphabetic() => self.read_letter_token(),
                    _ => self.read_bad_token(c),
                };

                token
            }
            None => SyntaxToken::new(SyntaxTokenContent::EOF, TextSpan::new(self.position, 0)),
        }
    }

    fn read_bad_token(&mut self, c: char) -> SyntaxToken {
        self.diagnostics.report_bad_character(self.position, c);
        self.next();
        SyntaxToken::new(
            SyntaxTokenContent::BadToken,
            TextSpan::new(self.position - 1, 1),
        )
    }

    fn read_bang_token(&mut self) -> SyntaxToken {
        if let Some(c) = self.look_ahead() {
            if c == '=' {
                let span = TextSpan::new(self.position, 2);
                self.next();
                self.next();
                return SyntaxToken::new(
                    SyntaxTokenContent::Binary(Some(BinaryToken::new(
                        BinaryTokenKind::BangEquals,
                        span,
                    ))),
                    span,
                );
            }
        }
        let span = TextSpan::new(self.position, 1);
        self.next();
        return SyntaxToken::new(
            SyntaxTokenContent::Binary(Some(BinaryToken::new(BinaryTokenKind::Bang, span))),
            span,
        );
    }

    fn read_equals_token(&mut self) -> SyntaxToken {
        if let Some(c) = self.look_ahead() {
            if c == '=' {
                let span = TextSpan::new(self.position, 2);
                self.next();
                self.next();
                return SyntaxToken::new(
                    SyntaxTokenContent::Binary(Some(BinaryToken::new(
                        BinaryTokenKind::EqualsEquals,
                        span,
                    ))),
                    span,
                );
            }
        }
        let span = TextSpan::new(self.position, 1);
        self.next();
        return SyntaxToken::new(SyntaxTokenContent::Equals, span);
    }

    fn read_pipe_token(&mut self) -> SyntaxToken {
        if let Some(c) = self.look_ahead() {
            if c == '|' {
                let span = TextSpan::new(self.position, 2);
                self.next();
                self.next();
                return SyntaxToken::new(
                    SyntaxTokenContent::Binary(Some(BinaryToken::new(
                        BinaryTokenKind::PipePipe,
                        span,
                    ))),
                    span,
                );
            }
        }

        let span = TextSpan::new(self.position, 1);
        self.next();

        return SyntaxToken::new(
            SyntaxTokenContent::Binary(Some(BinaryToken::new(BinaryTokenKind::Pipe, span))),
            span,
        );
    }

    fn read_ampersand_token(&mut self) -> SyntaxToken {
        if let Some(c) = self.look_ahead() {
            if c == '&' {
                let span = TextSpan::new(self.position, 2);
                self.next();
                self.next();
                return SyntaxToken::new(
                    SyntaxTokenContent::Binary(Some(BinaryToken::new(
                        BinaryTokenKind::AmpersandAmpersand,
                        span,
                    ))),
                    span,
                );
            }
        }

        let span = TextSpan::new(self.position, 1);
        self.next();
        return SyntaxToken::new(
            SyntaxTokenContent::Binary(Some(BinaryToken::new(BinaryTokenKind::Ampersand, span))),
            span,
        );
    }

    fn read_bracket_token(&mut self, kind: Parenthesis) -> SyntaxToken {
        self.next();

        SyntaxToken::new(
            SyntaxTokenContent::Bracket { kind },
            TextSpan::new(self.position - 1, 1),
        )
    }

    fn read_brace_token(&mut self, kind: Parenthesis) -> SyntaxToken {
        self.next();

        SyntaxToken::new(
            SyntaxTokenContent::Brace { kind },
            TextSpan::new(self.position - 1, 1),
        )
    }

    fn read_simple_binary_token(&mut self, kind: BinaryTokenKind) -> SyntaxToken {
        let span = TextSpan::new(self.position, 1);
        self.next();

        SyntaxToken::new(
            SyntaxTokenContent::Binary(Some(BinaryToken::new(kind, span))),
            span,
        )
    }

    fn read_letter_token(&mut self) -> SyntaxToken {
        let mut token_text = String::new();
        let start_position = self.position;
        loop {
            match self.current_char() {
                Some(c) => {
                    if !c.is_alphanumeric() {
                        break;
                    }

                    token_text.push(c);
                    self.next();
                }
                None => break,
            };
        }
        let keyword = Keyword::from_str(token_text.as_str());

        if let Some(keyword) = keyword {
            SyntaxToken::new(
                SyntaxTokenContent::Keyword(keyword),
                TextSpan::new(start_position, token_text.len()),
            )
        } else {
            let length = token_text.len();

            SyntaxToken::new(
                SyntaxTokenContent::Identifier(token_text),
                TextSpan::new(start_position, length),
            )
        }
    }

    fn read_whitespace_token(&mut self) -> SyntaxToken {
        let mut token_text = String::new();
        let start_position = self.position;
        loop {
            match self.current_char() {
                Some(c) => {
                    if !c.is_whitespace() {
                        break;
                    }

                    token_text.push(c);
                    self.next();
                }
                None => break,
            };
        }

        SyntaxToken::new(
            SyntaxTokenContent::Whitespace,
            TextSpan::new(start_position, self.position - start_position - 1),
        )
    }

    fn read_number_token(&mut self) -> SyntaxToken {
        let mut token_text = String::new();
        let start_position = self.position;
        loop {
            match self.current_char() {
                Some(c) => {
                    if !c.is_numeric() {
                        break;
                    }

                    token_text.push(c);
                    self.next();
                }
                None => break,
            };
        }

        let num = token_text.parse::<isize>();
        if let Ok(num) = num {
            return SyntaxToken::new(
                SyntaxTokenContent::Number(Some(num)),
                TextSpan::new(start_position, token_text.len()),
            );
        } else {
            let span = TextSpan::new(self.position, token_text.len());
            self.diagnostics.report_invalid_number(
                span,
                token_text.as_str(),
                Type::Integer(IntegerType::I32),
            );
            return SyntaxToken::new(SyntaxTokenContent::BadToken, span);
        }
    }

    fn next(&mut self) {
        self.position += 1;
    }

    fn current_char(&self) -> Option<char> {
        self.peek(0)
    }

    fn look_ahead(&self) -> Option<char> {
        self.peek(1)
    }

    fn peek(&self, offset: usize) -> Option<char> {
        let index = self.position + offset;

        if index >= self.chars.len() {
            None
        } else {
            Some(self.chars[index])
        }
    }
}

// #[cfg(test)]
// mod tests {
//     use crate::syntax::{
//         lexer::Lexer,
//         tokens::{SyntaxToken, SyntaxTokenContent},
//     };
//     use std::matches;

//     #[test]
//     fn parses_correct_number_of_tokens() {
//         let text = "abc";
//         let result = parse_tokens(text);

//         assert_eq!(result.len(), 1);

//         let token = &result[0];

//         assert!(matches!(token.token, SyntaxTokenContent::Identifier(_)));
//         if let SyntaxTokenContent::Identifier(id) = &token.token {
//             assert_eq!(text, id.as_str());
//         };
//     }

//     fn parse_tokens(text: &str) -> Vec<SyntaxToken> {
//         let mut lexer = Lexer::new(text);
//         let mut tokens = vec![];
//         loop {
//             let token = lexer.lex();
//             if let SyntaxTokenContent::EOF = token.token {
//                 break;
//             } else {
//                 tokens.push(token.clone());
//             }
//         }

//         return tokens;
//     }

//     macro_rules! token_tests {
//         ( $( $value:expr ),* ) => {
//             $(
//                 #[test]
//                 fn hello() {
//                     let (input, expected) = $value;
//                     assert_eq!(expected, input);
//                 }
//             )*
//         }
//     }

//     token_tests![(1, 1)];
// }

use super::tokens::{ExpressionSyntax, SyntaxToken};

#[derive(Clone)]
pub enum StatementSyntax {
    Block {
        open_brace: SyntaxToken,
        statements: Vec<Box<StatementSyntax>>,
        close_brace: SyntaxToken,
    },
    Expression {
        expression: ExpressionSyntax,
    },
}

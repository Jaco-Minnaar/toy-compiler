use std::fmt::Display;

use crate::diagnostic::TextSpan;

#[derive(Debug, Clone)]
pub enum SyntaxTokenContent {
    EOF,
    Def,
    Identifier(String),
    Number(Option<isize>),
    Whitespace,
    Binary(Option<BinaryToken>),
    Bracket { kind: Parenthesis },
    Brace { kind: Parenthesis },
    Equals,
    Keyword(Keyword),
    BadToken,
    CompilationUnit(Box<ExpressionSyntax>, Box<SyntaxToken>),
}

#[derive(Debug, Clone, Copy)]
pub enum Parenthesis {
    Open,
    Close,
}

#[derive(Debug, Clone, Copy)]
pub enum Keyword {
    True,
    False,
}

impl Keyword {
    pub fn from_str(word: &str) -> Option<Keyword> {
        if word == "true" {
            return Some(Keyword::True);
        }

        if word == "false" {
            return Some(Keyword::False);
        }

        None
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum BinaryTokenKind {
    Plus,
    Minus,
    Star,
    Slash,
    Ampersand,
    Pipe,
    AmpersandAmpersand,
    PipePipe,
    Bang,
    EqualsEquals,
    BangEquals,
}

#[derive(Debug, Clone, Copy)]
pub struct BinaryToken {
    pub token_kind: BinaryTokenKind,
    pub span: TextSpan,
}

impl BinaryToken {
    pub fn new(token_kind: BinaryTokenKind, span: TextSpan) -> Self {
        BinaryToken { token_kind, span }
    }

    pub fn binary_precedence(&self) -> usize {
        match self.token_kind {
            BinaryTokenKind::Slash | BinaryTokenKind::Star => 6,
            BinaryTokenKind::Plus | BinaryTokenKind::Minus => 5,
            BinaryTokenKind::EqualsEquals | BinaryTokenKind::BangEquals => 4,
            BinaryTokenKind::Ampersand | BinaryTokenKind::Pipe => 3,
            BinaryTokenKind::AmpersandAmpersand => 2,
            BinaryTokenKind::PipePipe => 1,
            BinaryTokenKind::Bang => 0,
        }
    }

    pub fn unary_precedence(&self) -> usize {
        match self.token_kind {
            BinaryTokenKind::Minus | BinaryTokenKind::Plus | BinaryTokenKind::Bang => 7,
            _ => 0,
        }
    }
}

impl Display for BinaryToken {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "BinaryToken {:?}", self.token_kind)
    }
}

#[derive(Debug, Clone)]
pub struct SyntaxToken {
    pub token: SyntaxTokenContent,
    pub span: TextSpan,
}

impl SyntaxToken {
    pub fn new(token: SyntaxTokenContent, span: TextSpan) -> Self {
        SyntaxToken { token, span }
    }
}

#[derive(Debug, Clone, Copy)]
pub enum LiteralExpression {
    I32(i32),
    Boolean(bool),
}

#[derive(Debug, Clone)]
pub enum ExpressionSyntax {
    Binary(Box<ExpressionSyntax>, BinaryToken, Box<ExpressionSyntax>),
    Literal(LiteralExpression),
    Parenthesized(Option<Box<ExpressionSyntax>>),
    Unary(BinaryToken, Box<ExpressionSyntax>),
    Assignment(SyntaxToken, Box<ExpressionSyntax>),
    Name(SyntaxToken),
}

impl Display for ExpressionSyntax {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                &ExpressionSyntax::Binary(_, _, _) => "BinaryExpression",
                &ExpressionSyntax::Literal(_) => "LiteralExpression",
                &ExpressionSyntax::Parenthesized(_) => "ParenthesizedExpression",
                &ExpressionSyntax::Unary(_, _) => "UnaryExpression",
                &ExpressionSyntax::Assignment(_, _) => "AssignmentExpression",
                &ExpressionSyntax::Name(_) => "NameExpression",
            }
        )
    }
}

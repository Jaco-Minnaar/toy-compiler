use super::{
    statement_syntax::StatementSyntax,
    tokens::{ExpressionSyntax, SyntaxToken},
};

#[derive(Clone)]
pub struct CompilationUnit {
    pub statement: StatementSyntax,
    pub end_of_file_token: SyntaxToken,
}

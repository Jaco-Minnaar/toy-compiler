use std::{cell::RefCell, convert::TryInto};

use crate::{
    diagnostic::DiagnosticReport,
    syntax::{lexer::Lexer, tokens::SyntaxTokenContent},
    text::source_text::SourceText,
};

use super::{
    compilation_unit::CompilationUnit,
    statement_syntax::StatementSyntax,
    tokens::{ExpressionSyntax, Keyword, LiteralExpression, Parenthesis, SyntaxToken},
};

pub struct Parser {
    tokens: Vec<SyntaxToken>,
    pub diagnostics: RefCell<DiagnosticReport>,
}

impl Parser {
    pub fn new(text: &SourceText) -> Parser {
        let mut tokens: Vec<SyntaxToken> = vec![];
        let mut diagnostics = DiagnosticReport::new();
        let mut lexer = Lexer::new(text.clone());

        loop {
            let token = lexer.lex();

            if let SyntaxTokenContent::Whitespace = token.token {
                continue;
            }

            if let SyntaxTokenContent::BadToken = token.token {
                continue;
            }

            let is_eof = if let SyntaxTokenContent::EOF = token.token {
                true
            } else {
                false
            };

            tokens.push(token);

            if is_eof {
                break;
            }
        }

        diagnostics.append_report(&lexer.diagnostics);

        Parser {
            tokens,
            diagnostics: RefCell::new(diagnostics),
        }
    }

    pub fn parse_compilation_unit(&mut self) -> CompilationUnit {
        let (statement, position) = self.parse_statement(0);

        let current_token = self.current_token(position);

        let eof_token = if let SyntaxTokenContent::EOF = current_token.token {
            current_token.clone()
        } else {
            let mut diags = self.diagnostics.borrow_mut();
            diags.report_unexpected_token(
                current_token.span,
                &current_token.token,
                &SyntaxTokenContent::EOF,
            );
            SyntaxToken::new(SyntaxTokenContent::EOF, current_token.span)
        };

        CompilationUnit {
            statement,
            end_of_file_token: eof_token,
        }
    }

    fn parse_statement(&self, position: usize) -> (StatementSyntax, usize) {
        let current_token = self.current_token(position);

        return if let SyntaxTokenContent::Brace {
            kind: Parenthesis::Open,
        } = current_token.token
        {
            self.parse_block_statement(position)
        } else {
            self.parse_expression_statement(position)
        };
    }

    fn parse_block_statement(&self, position: usize) -> (StatementSyntax, usize) {
        todo!()
    }

    fn parse_expression_statement(&self, position: usize) -> (StatementSyntax, usize) {
        let (expression, new_position) = self.parse_expression(position);

        (StatementSyntax::Expression { expression }, new_position)
    }

    fn parse_expression(&self, position: usize) -> (ExpressionSyntax, usize) {
        self.parse_assignment_expression(position)
    }

    fn parse_assignment_expression(&self, position: usize) -> (ExpressionSyntax, usize) {
        let mut position = position;

        let current_token = self.current_token(position);
        let next_token = self.peek(1, position);
        if let SyntaxTokenContent::Identifier(_) = current_token.token {
            if let SyntaxTokenContent::Equals = next_token.token {
                position += 2;

                let (right, new_position) = self.parse_assignment_expression(position);
                position = new_position;
                return (
                    ExpressionSyntax::Assignment(current_token.clone(), Box::new(right)),
                    position,
                );
            }
        }

        self.parse_binary_expression(None, position)
    }

    fn parse_binary_expression(
        &self,
        parent_precedence: Option<usize>,
        position: usize,
    ) -> (ExpressionSyntax, usize) {
        let parent_precedence = parent_precedence.unwrap_or(0);
        let mut position = position;
        let mut left = None;
        let current_token = self.current_token(position);

        if let SyntaxTokenContent::Binary(token) = current_token.token {
            let token = token.unwrap();
            position += 1;
            let unary_precedence = token.unary_precedence();
            if unary_precedence != 0 && unary_precedence >= parent_precedence {
                let (right, new_position) =
                    self.parse_binary_expression(Some(unary_precedence), position);
                position = new_position;
                left = Some(ExpressionSyntax::Unary(token, Box::new(right)));
            }
        }

        if let None = left {
            let (new_left, new_position) = self.parse_primary_expression(position);
            left = Some(new_left);
            position = new_position;
        }

        loop {
            let operator_token = self.current_token(position);
            match operator_token.token {
                SyntaxTokenContent::Binary(operator_token) => {
                    if let Some(operator_token) = operator_token {
                        let precedence = operator_token.binary_precedence();

                        if precedence == 0 || precedence < parent_precedence {
                            break;
                        }

                        position += 1;
                        let (right, new_position) =
                            self.parse_binary_expression(Some(precedence), position);
                        position = new_position;
                        left = Some(ExpressionSyntax::Binary(
                            Box::new(left.unwrap()),
                            operator_token,
                            Box::new(right),
                        ));
                    } else {
                        println!("operator_token None");
                        break;
                    }
                }
                _ => {
                    // println!("Syntax token not binary");
                    break;
                }
            }
        }

        return (left.unwrap(), position);
    }

    fn peek(&self, offset: usize, position: usize) -> &SyntaxToken {
        let peek_pos = position + offset;

        if peek_pos >= self.tokens.len() {
            return self.tokens.last().unwrap();
        }

        return &self.tokens[peek_pos];
    }

    fn current_token(&self, position: usize) -> &SyntaxToken {
        self.peek(0, position)
    }

    fn parse_primary_expression(&self, position: usize) -> (ExpressionSyntax, usize) {
        let mut position = position;
        let current_token = self.current_token(position);

        match current_token.token {
            SyntaxTokenContent::Bracket {
                kind: Parenthesis::Open,
            } => {
                position += 1;
                let (expression, new_position) = self.parse_expression(position);
                position = new_position;

                let current_token = self.current_token(position);
                if let SyntaxTokenContent::Bracket {
                    kind: Parenthesis::Close,
                } = current_token.token
                {
                    (
                        ExpressionSyntax::Parenthesized(Some(Box::new(expression))),
                        position + 1,
                    )
                } else {
                    let mut diagnostics = self.diagnostics.borrow_mut();
                    diagnostics.report_unexpected_token(
                        current_token.span,
                        &current_token.token,
                        &SyntaxTokenContent::Bracket {
                            kind: Parenthesis::Close,
                        },
                    );

                    (ExpressionSyntax::Parenthesized(None), position)
                }
            }
            SyntaxTokenContent::Keyword(keyword) => {
                position += 1;

                (
                    ExpressionSyntax::Literal(LiteralExpression::Boolean(match keyword {
                        Keyword::False => false,
                        Keyword::True => true,
                    })),
                    position,
                )
            }
            SyntaxTokenContent::Identifier(_) => {
                position += 1;

                (ExpressionSyntax::Name(current_token.clone()), position)
            }
            SyntaxTokenContent::Number(num) => {
                position += 1;

                (
                    ExpressionSyntax::Literal(LiteralExpression::I32(
                        num.unwrap_or(0).try_into().unwrap(),
                    )),
                    position,
                )
            }
            _ => {
                let mut diagnostics = self.diagnostics.borrow_mut();
                diagnostics.report_unexpected_token(
                    current_token.span,
                    &current_token.token,
                    &SyntaxTokenContent::Number(None),
                );

                (
                    ExpressionSyntax::Literal(LiteralExpression::I32(0)),
                    position,
                )
            }
        }
    }
}

use std::collections::HashMap;

use crate::{
    binding::{binder::Binder, bound_global_scope::BoundGlobalScope},
    diagnostic::DiagnosticReport,
    evaluator::{evaluator::Evaluator, values::Value},
    syntax::syntax_tree::SyntaxTree,
};

pub struct EvaluationResult {
    pub diagnostics: DiagnosticReport,
    pub value: Option<Value>,
}

impl EvaluationResult {
    pub fn new(diagnostics: DiagnosticReport, value: Option<Value>) -> Self {
        EvaluationResult { diagnostics, value }
    }
}

#[derive(Clone)]
pub struct Compilation {
    global_scope: BoundGlobalScope,
    pub syntax_tree: SyntaxTree,
}

impl Compilation {
    pub fn new(syntax_tree: SyntaxTree) -> Self {
        Compilation {
            global_scope: Binder::bind_global_scope(None, &syntax_tree.root),
            syntax_tree,
        }
    }

    fn from_previous(previous: Compilation, syntax_tree: SyntaxTree) -> Self {
        let previous_scope = previous.global_scope;

        let new_scope = Binder::bind_global_scope(Some(previous_scope), &syntax_tree.root);

        Compilation {
            global_scope: new_scope,
            syntax_tree,
        }
    }

    pub fn continue_with(self, syntax_tree: SyntaxTree) -> Self {
        Self::from_previous(self, syntax_tree)
    }

    pub fn evaluate(&self, variables: &mut HashMap<String, Value>) -> EvaluationResult {
        let mut diagnostics = DiagnosticReport::new();
        diagnostics.append_report(&self.global_scope.diagnostics);

        if !diagnostics.see_diagnostics().is_empty() {
            return EvaluationResult::new(diagnostics, None);
        }

        let mut evaluator = Evaluator::new();
        let result = evaluator.evaluate_expression(&self.global_scope.expression, variables);

        match result {
            Ok(value) => return EvaluationResult::new(diagnostics, Some(value)),
            Err(_) => {
                todo!();
            }
        }
    }
}

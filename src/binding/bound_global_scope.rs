use crate::diagnostic::DiagnosticReport;

use super::{bound_expression::BoundExpression, bound_scope::BoundScope};

#[derive(Clone)]
pub struct BoundGlobalScope {
    pub previous: Option<Box<BoundGlobalScope>>,
    pub diagnostics: DiagnosticReport,
    pub variables: BoundScope,
    pub expression: BoundExpression,
}

use crate::{
    syntax::tokens::BinaryTokenKind,
    types::{IntegerType, Type},
};

use lazy_static::lazy_static;

#[derive(Debug, Clone, Copy)]
pub enum BoundUnaryOperatorKind {
    Identity,
    Negation,
    LogicalNegation,
}

#[derive(Debug, Clone, Copy)]
pub struct BoundUnaryOperator {
    pub binary_token: BinaryTokenKind,
    pub kind: BoundUnaryOperatorKind,
    pub operand_type: Type,
    pub result_type: Type,
}

impl BoundUnaryOperator {
    fn new(
        binary_token: BinaryTokenKind,
        kind: BoundUnaryOperatorKind,
        operand_type: Type,
        result_type: Type,
    ) -> Self {
        BoundUnaryOperator {
            binary_token,
            kind,
            operand_type,
            result_type,
        }
    }

    fn with_only_operand_type(
        binary_token: BinaryTokenKind,
        kind: BoundUnaryOperatorKind,
        operand_type: Type,
    ) -> Self {
        Self::new(binary_token, kind, operand_type, operand_type)
    }
}

lazy_static! {
    static ref BOUND_UNARY_OPERATORS: Vec<BoundUnaryOperator> = vec![
        BoundUnaryOperator::with_only_operand_type(
            BinaryTokenKind::Bang,
            BoundUnaryOperatorKind::LogicalNegation,
            Type::Boolean
        ),
        BoundUnaryOperator::with_only_operand_type(
            BinaryTokenKind::Plus,
            BoundUnaryOperatorKind::Identity,
            Type::Integer(IntegerType::I32)
        ),
        BoundUnaryOperator::with_only_operand_type(
            BinaryTokenKind::Minus,
            BoundUnaryOperatorKind::Negation,
            Type::Integer(IntegerType::I32)
        )
    ];
}

pub fn bind_unary_operator(
    token: BinaryTokenKind,
    operand_type: Type,
) -> Option<&'static BoundUnaryOperator> {
    BOUND_UNARY_OPERATORS
        .iter()
        .find(|value| value.binary_token == token && value.operand_type == operand_type)
}

use lazy_static::lazy_static;

use crate::{
    syntax::tokens::BinaryTokenKind,
    types::{IntegerType, Type},
};

#[derive(Debug, Clone, Copy)]
pub enum BoundBinaryOperatorKind {
    Addition,
    Subtraction,
    Multiplication,
    Division,
    LogicalAnd,
    LogicalOr,
    Equals,
    NotEquals,
}

#[derive(Debug, Clone, Copy)]
pub struct BoundBinaryOperator {
    pub syntax_token: BinaryTokenKind,
    pub kind: BoundBinaryOperatorKind,
    pub left_type: Type,
    pub right_type: Type,
    pub result_type: Type,
}

impl BoundBinaryOperator {
    fn new(
        syntax_token: BinaryTokenKind,
        kind: BoundBinaryOperatorKind,
        left_type: Type,
        right_type: Type,
        result_type: Type,
    ) -> Self {
        BoundBinaryOperator {
            syntax_token,
            kind,
            left_type,
            right_type,
            result_type,
        }
    }

    fn with_general_type(
        syntax_token: BinaryTokenKind,
        kind: BoundBinaryOperatorKind,
        general_type: Type,
    ) -> Self {
        BoundBinaryOperator::new(syntax_token, kind, general_type, general_type, general_type)
    }

    fn with_operand_type(
        syntax_token: BinaryTokenKind,
        kind: BoundBinaryOperatorKind,
        operand_type: Type,
        result_type: Type,
    ) -> Self {
        BoundBinaryOperator::new(syntax_token, kind, operand_type, operand_type, result_type)
    }
}

lazy_static! {
    static ref BOUND_BINARY_OPERATORS: Vec<BoundBinaryOperator> = vec![
        BoundBinaryOperator::with_general_type(
            BinaryTokenKind::Plus,
            BoundBinaryOperatorKind::Addition,
            Type::Integer(IntegerType::I32),
        ),
        BoundBinaryOperator::with_general_type(
            BinaryTokenKind::Minus,
            BoundBinaryOperatorKind::Subtraction,
            Type::Integer(IntegerType::I32),
        ),
        BoundBinaryOperator::with_general_type(
            BinaryTokenKind::Star,
            BoundBinaryOperatorKind::Multiplication,
            Type::Integer(IntegerType::I32),
        ),
        BoundBinaryOperator::with_general_type(
            BinaryTokenKind::Slash,
            BoundBinaryOperatorKind::Division,
            Type::Integer(IntegerType::I32),
        ),
        BoundBinaryOperator::with_operand_type(
            BinaryTokenKind::EqualsEquals,
            BoundBinaryOperatorKind::Equals,
            Type::Integer(IntegerType::I32),
            Type::Boolean
        ),
        BoundBinaryOperator::with_general_type(
            BinaryTokenKind::EqualsEquals,
            BoundBinaryOperatorKind::Equals,
            Type::Boolean
        ),
        BoundBinaryOperator::with_operand_type(
            BinaryTokenKind::BangEquals,
            BoundBinaryOperatorKind::NotEquals,
            Type::Integer(IntegerType::I32),
            Type::Boolean
        ),
        BoundBinaryOperator::with_general_type(
            BinaryTokenKind::BangEquals,
            BoundBinaryOperatorKind::NotEquals,
            Type::Boolean
        ),
        BoundBinaryOperator::with_general_type(
            BinaryTokenKind::AmpersandAmpersand,
            BoundBinaryOperatorKind::LogicalAnd,
            Type::Boolean,
        ),
        BoundBinaryOperator::with_general_type(
            BinaryTokenKind::PipePipe,
            BoundBinaryOperatorKind::LogicalOr,
            Type::Boolean,
        ),
    ];
}

pub fn bind_binary_operator(
    token: &BinaryTokenKind,
    left_type: &Type,
    right_type: &Type,
) -> Option<&'static BoundBinaryOperator> {
    BOUND_BINARY_OPERATORS.iter().find(|value| {
        &value.syntax_token == token
            && &value.left_type == left_type
            && &value.right_type == right_type
    })
}

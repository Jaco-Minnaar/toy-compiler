use crate::{
    diagnostic::DiagnosticReport,
    syntax::{
        compilation_unit::CompilationUnit,
        tokens::{
            BinaryToken, ExpressionSyntax, LiteralExpression, SyntaxToken, SyntaxTokenContent,
        },
    },
    variable_symbol::VariableSymbol,
};

use super::{
    bound_binary_operator::{self, BoundBinaryOperator},
    bound_expression::{BoundExpression, BoundLiteralExpression, Integer, Typed},
    bound_global_scope::BoundGlobalScope,
    bound_scope::BoundScope,
    bound_unary_operator,
};

#[derive(Debug)]
pub enum BindError {
    UnexpectedExpression,
    EmptyLiteralExpression,
    EmptyParenthesizedExpression,
    VariableAlreadyExists,
    UnexpectedUnaryOperator(BinaryToken),
}

pub struct Binder {
    pub diagnostics: DiagnosticReport,
    pub bound_binary_operators: Vec<BoundBinaryOperator>,
    bound_scope: BoundScope,
}

impl Binder {
    pub fn new() -> Self {
        Binder {
            diagnostics: DiagnosticReport::new(),
            bound_binary_operators: vec![],
            bound_scope: BoundScope::new(),
        }
    }

    pub fn with_bound_scope(bound_scope: BoundScope) -> Self {
        Binder {
            diagnostics: DiagnosticReport::new(),
            bound_binary_operators: vec![],
            bound_scope: BoundScope::from_parent(bound_scope),
        }
    }

    pub fn bind_global_scope(
        previous: Option<BoundGlobalScope>,
        syntax: &CompilationUnit,
    ) -> BoundGlobalScope {
        let (mut binder, previous) = if let Some(previous_global_scope) = previous {
            let boxed_previous = Box::new(previous_global_scope);

            let parent_scope = Self::create_parent_scopes(&boxed_previous);
            (Self::with_bound_scope(parent_scope), Some(boxed_previous))
        } else {
            (Self::new(), None)
        };

        let expression = binder.bind_expression(&syntax.statement).unwrap();
        let mut diagnostics = DiagnosticReport::new();
        diagnostics.append_report(&binder.diagnostics);

        let global_scope = BoundGlobalScope {
            previous,
            diagnostics,
            variables: binder.bound_scope,
            expression,
        };

        return global_scope;
    }

    fn create_parent_scopes(previous: &Box<BoundGlobalScope>) -> BoundScope {
        let mut previous = Some(previous);
        let mut stack: Vec<&Box<BoundGlobalScope>> = vec![];

        while let Some(previous_child) = previous {
            stack.push(previous_child);
            previous = previous_child.previous.as_ref();
        }

        let mut current = BoundScope::new();
        while let Some(previous_global) = stack.pop() {
            let mut scope = BoundScope::from_parent(current);
            for (_, variable) in previous_global.variables.variables.iter() {
                scope.try_declare(variable.clone()).unwrap_or_default();
            }

            current = scope;
        }

        current
    }

    pub fn bind_expression(
        &mut self,
        expression: &ExpressionSyntax,
    ) -> Result<BoundExpression, BindError> {
        match expression {
            ExpressionSyntax::Binary(left, operator, right) => {
                self.bind_binary(left, operator, right)
            }

            ExpressionSyntax::Literal(expression) => self.bind_literal(expression),
            ExpressionSyntax::Unary(operator, operand) => self.bind_unary(operator, operand),
            ExpressionSyntax::Parenthesized(expression) => match expression {
                Some(expression) => self.bind_expression(&expression),
                _ => Err(BindError::EmptyParenthesizedExpression),
            },
            ExpressionSyntax::Name(token) => self.bind_name(token),

            ExpressionSyntax::Assignment(identifier, expression) => {
                self.bind_assignment(identifier, expression)
            }
        }
    }

    fn bind_binary(
        &mut self,
        left: &ExpressionSyntax,
        operator: &BinaryToken,
        right: &ExpressionSyntax,
    ) -> Result<BoundExpression, BindError> {
        let bound_left = self.bind_expression(left)?;
        let bound_right = self.bind_expression(right)?;

        let left_type = bound_left.get_type();
        let right_type = bound_right.get_type();

        let bound_binary_operator = bound_binary_operator::bind_binary_operator(
            &operator.token_kind,
            &left_type,
            &right_type,
        );

        if let Some(bound_binary_operator) = bound_binary_operator {
            return Ok(BoundExpression::Binary(
                Box::new(bound_left),
                bound_binary_operator,
                Box::new(bound_right),
            ));
        } else {
            self.diagnostics.report_undefined_binary_operator(
                operator.span,
                &operator.token_kind,
                &left_type,
                &right_type,
            );
            return Ok(bound_left);
        }
    }

    fn bind_literal(
        &mut self,
        expression: &LiteralExpression,
    ) -> Result<BoundExpression, BindError> {
        return match expression {
            &LiteralExpression::Boolean(boolean) => Ok(BoundExpression::Literal(
                BoundLiteralExpression::Boolean(boolean),
            )),
            &LiteralExpression::I32(num) => Ok(BoundExpression::Literal(
                BoundLiteralExpression::Integer(Integer::I32(num)),
            )),
        };
    }

    fn bind_unary(
        &mut self,
        operator: &BinaryToken,
        operand: &ExpressionSyntax,
    ) -> Result<BoundExpression, BindError> {
        let bound_operand = self.bind_expression(operand)?;

        let operand_type = bound_operand.get_type();
        let bound_operator =
            bound_unary_operator::bind_unary_operator(operator.token_kind, operand_type);

        if let Some(bound_operator) = bound_operator {
            Ok(BoundExpression::Unary(
                bound_operator,
                Box::new(bound_operand),
            ))
        } else {
            self.diagnostics.report_undefined_unary_operator(
                operator.span,
                &operator.token_kind,
                &operand_type,
            );
            return Ok(bound_operand);
        }
    }

    fn bind_name(&mut self, token: &SyntaxToken) -> Result<BoundExpression, BindError> {
        if let SyntaxTokenContent::Identifier(identifier) = &token.token {
            if let Some(expression_type) = self.bound_scope.try_lookup(identifier.as_str()) {
                return Ok(BoundExpression::Variable(expression_type.clone()));
            } else {
                self.diagnostics
                    .report_undefined_name(token.span, identifier.as_str());
                return Ok(BoundExpression::Literal(BoundLiteralExpression::Integer(
                    Integer::I32(0),
                )));
            }
        } else {
            panic!(
                "Code should never reach here as token has already been checked to be Identifier"
            );
        }
    }

    fn bind_assignment(
        &mut self,
        identifier: &SyntaxToken,
        expression: &Box<ExpressionSyntax>,
    ) -> Result<BoundExpression, BindError> {
        if let SyntaxTokenContent::Identifier(identifier_name) = &identifier.token {
            let bound_expression = self.bind_expression(expression)?;

            let variable = if let Some(variable) = self.bound_scope.try_lookup(identifier_name) {
                variable.clone()
            } else {
                let variable = VariableSymbol {
                    name: identifier_name.clone(),
                    symbol_type: bound_expression.get_type(),
                };
                self.bound_scope
                    .try_declare(variable.clone())
                    .unwrap_or_default();
                variable
            };

            if bound_expression.get_type() != variable.symbol_type {
                self.diagnostics.report_cannot_convert(
                    identifier.span,
                    &bound_expression.get_type(),
                    &variable.symbol_type,
                );
                return Ok(bound_expression);
            }

            Ok(BoundExpression::Assignment(
                variable,
                Box::new(bound_expression),
            ))
        } else {
            panic!("Identifier token not identifier");
        }
    }
}

pub fn get_error_message(error: BindError) -> String {
    match error {
        BindError::EmptyLiteralExpression => "Unexpected empty literal token".to_owned(),
        BindError::UnexpectedExpression => {
            format!("Expected Binary, Unary, or Literal Expression. Got something")
        }
        BindError::UnexpectedUnaryOperator(operator) => {
            format!("Expected 'Plus' or 'Minus'. Got {:?}", operator)
        }
        BindError::EmptyParenthesizedExpression => {
            "Expected expression inside parenthesis. Found the void.".to_owned()
        }
        BindError::VariableAlreadyExists => "Variable already exists".to_owned(),
    }
}

use std::collections::HashMap;

use crate::variable_symbol::VariableSymbol;

#[derive(Clone)]
pub struct BoundScope {
    pub variables: HashMap<String, VariableSymbol>,
    parent: Option<Box<BoundScope>>,
}

impl BoundScope {
    pub fn new() -> Self {
        BoundScope {
            variables: HashMap::new(),
            parent: None,
        }
    }

    pub fn from_parent(parent: BoundScope) -> Self {
        BoundScope {
            variables: HashMap::new(),
            parent: Some(Box::new(parent)),
        }
    }

    pub fn try_declare(&mut self, variable: VariableSymbol) -> Result<(), String> {
        return if self.variables.contains_key(variable.name.as_str()) {
            Err("Variable already declared in this scope".to_string())
        } else {
            self.variables.insert(variable.name.clone(), variable);
            Ok(())
        };
    }

    pub fn try_lookup(&self, name: &str) -> Option<&VariableSymbol> {
        if let Some(variable) = self.variables.get(name) {
            return Some(variable);
        }

        if let Some(parent) = &self.parent {
            return parent.try_lookup(name);
        }

        return None;
    }
}

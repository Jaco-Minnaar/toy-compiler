use crate::{
    types::{IntegerType, Type},
    variable_symbol::VariableSymbol,
};

use super::{bound_binary_operator::BoundBinaryOperator, bound_unary_operator::BoundUnaryOperator};

pub trait Typed {
    fn get_type(&self) -> Type;
}

#[derive(Debug, Clone)]
pub enum BoundExpression {
    Literal(BoundLiteralExpression),
    Unary(&'static BoundUnaryOperator, Box<BoundExpression>),
    Binary(
        Box<BoundExpression>,
        &'static BoundBinaryOperator,
        Box<BoundExpression>,
    ),
    Variable(VariableSymbol),
    Assignment(VariableSymbol, Box<BoundExpression>),
}

impl Typed for BoundExpression {
    fn get_type(&self) -> Type {
        match self {
            BoundExpression::Binary(_, operator, _) => operator.result_type,
            BoundExpression::Unary(operator, _) => operator.result_type,
            BoundExpression::Literal(literal) => match literal {
                BoundLiteralExpression::Boolean(_) => Type::Boolean,
                BoundLiteralExpression::Integer(int) => match int {
                    Integer::I32(_) => Type::Integer(IntegerType::I32),
                },
            },
            BoundExpression::Variable(variable_symbol) => variable_symbol.symbol_type,
            BoundExpression::Assignment(_, expression) => expression.get_type(),
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub enum BoundLiteralExpression {
    Integer(Integer),
    Boolean(bool),
}

#[derive(Debug, Clone, Copy)]
pub enum Integer {
    I32(i32),
}

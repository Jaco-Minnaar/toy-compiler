use std::fmt::Display;

use crate::{
    syntax::tokens::{BinaryTokenKind, SyntaxTokenContent},
    types::Type,
};

#[derive(Clone, Copy, Debug)]
pub struct TextSpan {
    pub start: usize,
    pub length: usize,
}

impl TextSpan {
    pub fn new(start: usize, length: usize) -> Self {
        TextSpan { start, length }
    }

    pub fn end(&self) -> usize {
        self.start + self.length
    }
}

#[derive(Clone)]
pub struct Diagnostic {
    pub span: TextSpan,
    pub message: String,
}

impl Display for Diagnostic {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}: {}", self.span.start, self.message)
    }
}

impl Diagnostic {
    pub fn new(span: TextSpan, message: String) -> Self {
        Diagnostic {
            span,
            message: message.to_owned(),
        }
    }
}

#[derive(Clone)]
pub struct DiagnosticReport {
    diagnostics: Vec<Diagnostic>,
}

impl DiagnosticReport {
    pub fn new() -> Self {
        DiagnosticReport {
            diagnostics: vec![],
        }
    }

    pub fn append_report(&mut self, report: &DiagnosticReport) {
        self.diagnostics.extend_from_slice(report.see_diagnostics());
    }

    fn report(&mut self, span: TextSpan, message: String) {
        self.diagnostics.push(Diagnostic::new(span, message));
    }

    pub fn report_invalid_number(&mut self, span: TextSpan, text: &str, expected_type: Type) {
        let message = format!(
            "'{}' is not a valid representation of {:?}",
            text, expected_type
        );
        self.report(span, message);
    }

    pub fn report_bad_character(&mut self, position: usize, character: char) {
        let message = format!("Bad character input: {}", character);
        self.report(TextSpan::new(position, 1), message);
    }

    pub fn report_unexpected_token(
        &mut self,
        span: TextSpan,
        actual_type: &SyntaxTokenContent,
        expected_type: &SyntaxTokenContent,
    ) {
        let message = format!(
            "Expected type: {:?}. Got type: {:?}",
            expected_type, actual_type
        );

        self.report(span, message);
    }

    pub fn report_undefined_unary_operator(
        &mut self,
        span: TextSpan,
        operator_token: &BinaryTokenKind,
        operand_type: &Type,
    ) {
        let message = format!(
            "Unary operator {:?} is not defined for type {:?}",
            operator_token, operand_type
        );

        self.report(span, message);
    }

    pub fn report_undefined_binary_operator(
        &mut self,
        span: TextSpan,
        operator_token: &BinaryTokenKind,
        left_type: &Type,
        right_type: &Type,
    ) {
        let message = format!(
            "Binary operator {:?} not defined for types {:?} and {:?}",
            operator_token, left_type, right_type
        );

        self.report(span, message);
    }

    pub(crate) fn report_undefined_name(&mut self, span: TextSpan, name: &str) {
        let message = format!("Variable name {} does not exist", name);
        self.report(span, message);
    }

    pub fn report_variable_already_declared(&mut self, span: TextSpan, name: &str) {
        let message = format!("Variable name {} has already been declared", name);
        self.report(span, message)
    }

    pub(crate) fn report_cannot_convert(
        &mut self,
        span: TextSpan,
        from_type: &Type,
        to_type: &Type,
    ) {
        let message = format!("Cannot convert type {:?} to {:?}", from_type, to_type);
        self.report(span, message)
    }

    pub fn see_diagnostics(&self) -> &[Diagnostic] {
        &self.diagnostics
    }

    pub fn clear_diagnostics(&mut self) {
        self.diagnostics.clear();
    }
}

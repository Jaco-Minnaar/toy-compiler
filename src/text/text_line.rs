use crate::diagnostic::TextSpan;

#[derive(Clone, Copy)]
pub struct TextLine {
    pub start: usize,
    pub length: usize,
    pub length_with_line_break: usize,
    pub span: TextSpan,
    pub span_with_line_break: TextSpan,
}

impl TextLine {
    pub fn new(start: usize, length: usize, length_with_line_break: usize) -> Self {
        TextLine {
            start,
            length,
            length_with_line_break,
            span: TextSpan::new(start, length),
            span_with_line_break: TextSpan::new(start, length_with_line_break),
        }
    }

    pub fn end(&self) -> usize {
        self.start + self.length
    }
}

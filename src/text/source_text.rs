use std::str::Chars;

use crate::diagnostic::TextSpan;

use super::text_line::TextLine;

#[derive(Clone)]
pub struct SourceText {
    pub lines: Vec<TextLine>,
    text: String,
}

impl SourceText {
    pub fn new(text: String) -> Self {
        SourceText {
            lines: Self::parse_lines(text.as_str()),
            text,
        }
    }

    pub fn from_str(text: &str) -> Self {
        SourceText {
            lines: Self::parse_lines(text),
            text: text.to_owned(),
        }
    }

    fn parse_lines(text: &str) -> Vec<TextLine> {
        let mut result = vec![];
        let mut chars = text.chars().peekable();

        let mut position = 0;
        let mut line_start = 0;

        while let Some(chr) = chars.next() {
            let line_break_width = Self::calc_line_break_width(chr, chars.peek());

            if line_break_width == 0 {
                position += 1;
            } else {
                result.push(create_line(position, line_start, line_break_width));
                position += line_break_width;
                line_start = position;
            }
        }

        if position >= line_start {
            result.push(create_line(position, line_start, 0));
        }

        result
    }

    fn calc_line_break_width(current_char: char, next_char: Option<&char>) -> usize {
        if let Some(next_char) = next_char {
            if current_char == '\r' && next_char == &'\n' {
                return 2;
            }
        }

        if matches!(current_char, '\r' | '\n') {
            return 1;
        }

        return 0;
    }

    pub fn get_line_index(&self, position: usize) -> Option<usize> {
        let mut lower = 0;
        let mut upper = self.lines.len() - 1;

        while lower <= upper {
            let index = lower + (upper - lower) / 2;
            let current = &self.lines[index];
            let start = current.start;
            let end = current.end();

            if position < start {
                upper = index - 1;
            } else if position > end {
                lower = index + 1;
            } else {
                return Some(index);
            }
        }

        None
    }

    pub fn chars(&self) -> Chars {
        self.text.chars()
    }

    pub fn as_str(&self) -> &str {
        self.text.as_str()
    }

    pub fn range(&self, start: usize, end: usize) -> String {
        self.text
            .chars()
            .skip(start)
            .take(end - start)
            .collect::<String>()
    }

    pub fn to_string_from_span(&self, span: &TextSpan) -> String {
        self.range(span.start, span.start + span.length)
    }
}

fn create_line(position: usize, line_start: usize, line_break_width: usize) -> TextLine {
    let line_length = position - line_start;
    let line_length_w_break = line_length + line_break_width;

    TextLine::new(line_start, line_length, line_length_w_break)
}

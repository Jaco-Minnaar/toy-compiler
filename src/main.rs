use std::{
    collections::HashMap,
    io::{self, Write},
};

use rustyline::{error::ReadlineError, Editor};
use termcolor::{BufferWriter, Color, ColorChoice, ColorSpec, WriteColor};
use toy_compiler::{
    compilation::Compilation,
    diagnostic::DiagnosticReport,
    syntax::{
        syntax_tree::SyntaxTree,
        tokens::{ExpressionSyntax, LiteralExpression, SyntaxTokenContent},
    },
    text::source_text::SourceText,
};

fn main() {
    let mut show_tree = false;
    let mut input = String::new();
    let mut compilation: Option<Compilation> = None;
    let mut variables = HashMap::new();
    let mut rl = Editor::<()>::new();

    let buf_wtr = BufferWriter::stdout(ColorChoice::Always);
    loop {
        let mut buffer = buf_wtr.buffer();
        let prompt = if input.len() == 0 {
            buffer
                .set_color(ColorSpec::new().set_fg(Some(Color::Green)))
                .unwrap();
            ">> "
        } else {
            buffer
                .set_color(ColorSpec::new().set_fg(Some(Color::Yellow)))
                .unwrap();

            "*  "
        };

        let line = match rl.readline(prompt) {
            Ok(line) => line,
            Err(ReadlineError::Interrupted) | Err(ReadlineError::Eof) => {
                println!("Bye!");
                break;
            }
            Err(err) => {
                println!("Error: {:?}", err);
                break;
            }
        };
        // println!("{}", line);

        let is_empty = line.trim().is_empty();
        if input.len() == 0 {
            if is_empty {
                continue;
            }

            match line.trim() {
                "#tree" => {
                    show_tree = !show_tree;
                    println!("'show_tree' set to {}", show_tree);
                    continue;
                }
                "#clr" => {
                    print!("\x1B[2J\x1B[1;1H");
                    continue;
                }
                "#reset" => {
                    compilation = None;
                    continue;
                }
                _ => (),
            }
        }

        input.push_str(&line);
        let syntax_tree = SyntaxTree::parse_str(&input);

        if !is_empty && syntax_tree.diagnostics.see_diagnostics().len() > 0 {
            continue;
        }

        if syntax_tree.diagnostics.see_diagnostics().is_empty() {
            let old_compilation = compilation.take();
            let backup_compilation = old_compilation.clone();
            let tmp_compilation = if let Some(compilation) = old_compilation {
                compilation.continue_with(syntax_tree)
            } else {
                Compilation::new(syntax_tree)
            };

            let result = tmp_compilation.evaluate(&mut variables);

            if show_tree {
                pretty_print(&tmp_compilation.syntax_tree.root.statement, "", true);
                println!();
            }

            if let Some(value) = result.value {
                buffer
                    .set_color(ColorSpec::new().set_fg(Some(Color::Magenta)))
                    .unwrap();

                writeln!(&mut buffer, "{:?}", value).unwrap();
                buf_wtr.print(&buffer).unwrap();
                buffer.clear();
                buffer.reset().unwrap();
                buf_wtr.print(&buffer).unwrap();

                compilation = Some(tmp_compilation);
            } else {
                print_diagnostics(
                    &result.diagnostics,
                    &tmp_compilation.syntax_tree.source_text,
                    &buf_wtr,
                );
                compilation = backup_compilation;
            }
        } else {
            print_diagnostics(&syntax_tree.diagnostics, &syntax_tree.source_text, &buf_wtr);
        }

        io::stdout().flush().unwrap();
        input.clear();
    }
}

fn print_diagnostics(diagnostics: &DiagnosticReport, text: &SourceText, buf_wtr: &BufferWriter) {
    for diagnostic in diagnostics.see_diagnostics().iter() {
        let mut buffer = buf_wtr.buffer();
        let line_index = text
            .get_line_index(diagnostic.span.start)
            .unwrap_or_default();
        let line = text.lines[line_index];
        let line_number = line_index + 1;
        let character = diagnostic.span.start - line.start + 1;

        println!();

        buffer
            .set_color(ColorSpec::new().set_fg(Some(Color::Red)))
            .unwrap();

        writeln!(&mut buffer, "{}", diagnostic.message).unwrap();
        buf_wtr.print(&buffer).unwrap();
        buffer.clear();
        buffer.reset().unwrap();

        let prefix = substring(text.as_str(), line.start, diagnostic.span.start);
        let error = substring(text.as_str(), diagnostic.span.start, diagnostic.span.end());
        let suffix = substring(
            text.as_str(),
            diagnostic.span.end(),
            line.span_with_line_break.end(),
        );

        // println!("Stuff: {} {} {}", prefix, error, suffix);
        write!(&mut buffer, "({},{}):", line_number, character).unwrap();
        buf_wtr.print(&buffer).unwrap();
        buffer.clear();
        buffer.reset().unwrap();

        write!(&mut buffer, "    {}", prefix).unwrap();
        buf_wtr.print(&buffer).unwrap();
        buffer.clear();
        buffer.reset().unwrap();

        buffer
            .set_color(ColorSpec::new().set_fg(Some(Color::Red)))
            .unwrap();
        write!(&mut buffer, "{}", error).unwrap();
        buf_wtr.print(&buffer).unwrap();
        buffer.clear();
        buffer.reset().unwrap();

        write!(&mut buffer, "{}", suffix).unwrap();
        buf_wtr.print(&buffer).unwrap();
        buffer.clear();
        buffer.reset().unwrap();

        println!();
    }
    println!();
}

fn pretty_print(node: &ExpressionSyntax, indent: &str, is_last: bool) {
    // ├
    // │
    // └
    // ─

    match node {
        ExpressionSyntax::Binary(left, operator, right) => {
            println!(
                "{}{}BinaryExpression",
                indent,
                if is_last { "└─ " } else { "├─ " }
            );

            let mut indent_str = indent.to_owned();
            if is_last {
                indent_str.push(' ');
            } else {
                indent_str.push('│')
            }

            indent_str.push_str("   ");

            pretty_print(left, &indent_str, false);
            println!("{}├─ {}", indent_str, operator);

            pretty_print(right, &indent_str, true)
        }
        ExpressionSyntax::Unary(operator, operand) => {
            println!(
                "{}{}UnaryExpression",
                indent,
                if is_last { "└─ " } else { "├─ " },
            );

            let mut indent_str = indent.to_owned();
            if is_last {
                indent_str.push(' ');
            } else {
                indent_str.push('│')
            }

            indent_str.push_str("   ");
            println!("{}├─ {:?}", indent_str, operator);
            pretty_print(&operand, &indent_str, true);
        }
        ExpressionSyntax::Parenthesized(expression) => {
            if let Some(expression) = expression {
                println!(
                    "{}{}ParenthesizedExpression",
                    indent,
                    if is_last { "└─ " } else { "├─ " }
                );

                let mut indent_str = indent.to_owned();
                if is_last {
                    indent_str.push(' ');
                } else {
                    indent_str.push('│')
                }

                indent_str.push_str("   ");
                pretty_print(expression, &indent_str, true);
            }
        }
        ExpressionSyntax::Assignment(token, expression) => {
            if let SyntaxTokenContent::Identifier(name) = &token.token {
                println!(
                    "{}{}AssignmentExpression",
                    indent,
                    if is_last { "└─ " } else { "├─ " }
                );

                let mut indent_str = indent.to_owned();
                if is_last {
                    indent_str.push(' ');
                } else {
                    indent_str.push('│')
                }

                indent_str.push_str("   ");

                println!("{}├─ IdentifierToken {}", indent_str, name);

                pretty_print(expression, &indent_str, true)
            }
        }
        ExpressionSyntax::Name(token) => {
            if let SyntaxTokenContent::Identifier(name) = &token.token {
                let mut indent_str = indent.to_owned();
                if is_last {
                    indent_str.push_str("└─")
                } else {
                    indent_str.push_str("├─")
                }
                println!("{} {} {}", indent_str, node, name);
            }
        }
        ExpressionSyntax::Literal(literal) => {
            let mut indent_str = indent.to_owned();
            if is_last {
                indent_str.push_str("└─")
            } else {
                indent_str.push_str("├─")
            }
            match literal {
                LiteralExpression::I32(num) => println!("{} {} {}", indent_str, node, num),
                LiteralExpression::Boolean(boolean) => {
                    println!("{} {} {}", indent_str, node, boolean)
                }
            }
        }
    }
}

fn substring(string: &str, start: usize, end: usize) -> String {
    String::from_utf8(string.as_bytes()[start..end].to_vec()).unwrap()
}

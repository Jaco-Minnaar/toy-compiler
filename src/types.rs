#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum Type {
    Integer(IntegerType),
    Boolean,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum IntegerType {
    I32,
}

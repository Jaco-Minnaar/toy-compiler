#[derive(Debug, Clone, Copy)]
pub enum Value {
    Bool(bool),
    I32(i32),
}

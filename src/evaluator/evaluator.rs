use std::collections::HashMap;

use crate::binding::{
    bound_binary_operator::BoundBinaryOperatorKind,
    bound_expression::{BoundExpression, BoundLiteralExpression, Integer},
    bound_unary_operator::BoundUnaryOperatorKind,
};

use super::values::Value;

pub struct Evaluator {
    variables: HashMap<String, Value>,
}

impl Evaluator {
    pub fn new() -> Self {
        Evaluator {
            variables: HashMap::new(),
        }
    }

    pub fn evaluate_expression(
        &mut self,
        node: &BoundExpression,
        variables: &mut HashMap<String, Value>,
    ) -> Result<Value, EvalError> {
        match node {
            BoundExpression::Binary(left, operator, right) => {
                let left_result = self.evaluate_expression(&left, variables)?;
                let right_result = self.evaluate_expression(&right, variables)?;

                match operator.kind {
                    BoundBinaryOperatorKind::Addition => match left_result {
                        Value::I32(left) => match right_result {
                            Value::I32(right) => Ok(Value::I32(left + right)),
                            _ => Err(EvalError::UnexpectedValueType),
                        },
                        _ => Err(EvalError::UnexpectedValueType),
                    },
                    BoundBinaryOperatorKind::Subtraction => match left_result {
                        Value::I32(left) => match right_result {
                            Value::I32(right) => Ok(Value::I32(left - right)),
                            _ => Err(EvalError::UnexpectedValueType),
                        },
                        _ => Err(EvalError::UnexpectedValueType),
                    },
                    BoundBinaryOperatorKind::Multiplication => match left_result {
                        Value::I32(left) => match right_result {
                            Value::I32(right) => Ok(Value::I32(left * right)),
                            _ => Err(EvalError::UnexpectedValueType),
                        },
                        _ => Err(EvalError::UnexpectedValueType),
                    },
                    BoundBinaryOperatorKind::Division => match left_result {
                        Value::I32(left) => match right_result {
                            Value::I32(right) => Ok(Value::I32(left / right)),
                            _ => Err(EvalError::UnexpectedValueType),
                        },
                        _ => Err(EvalError::UnexpectedValueType),
                    },
                    BoundBinaryOperatorKind::LogicalAnd => match left_result {
                        Value::Bool(left) => match right_result {
                            Value::Bool(right) => Ok(Value::Bool(left && right)),
                            _ => Err(EvalError::UnexpectedValueType),
                        },
                        _ => Err(EvalError::UnexpectedValueType),
                    },
                    BoundBinaryOperatorKind::LogicalOr => match left_result {
                        Value::Bool(left) => match right_result {
                            Value::Bool(right) => Ok(Value::Bool(left || right)),
                            _ => Err(EvalError::UnexpectedValueType),
                        },
                        _ => Err(EvalError::UnexpectedValueType),
                    },
                    BoundBinaryOperatorKind::Equals => match left_result {
                        Value::Bool(left) => match right_result {
                            Value::Bool(right) => Ok(Value::Bool(left && right)),
                            _ => Err(EvalError::UnexpectedValueType),
                        },
                        Value::I32(left) => match right_result {
                            Value::I32(right) => Ok(Value::Bool(left == right)),
                            _ => Err(EvalError::UnexpectedValueType),
                        },
                    },
                    BoundBinaryOperatorKind::NotEquals => match left_result {
                        Value::Bool(left) => match right_result {
                            Value::Bool(right) => Ok(Value::Bool(!(left && right))),
                            _ => Err(EvalError::UnexpectedValueType),
                        },
                        Value::I32(left) => match right_result {
                            Value::I32(right) => Ok(Value::Bool(left != right)),
                            _ => Err(EvalError::UnexpectedValueType),
                        },
                    },
                }
            }
            BoundExpression::Variable(symbol) => Ok(*variables.get(symbol.name.as_str()).unwrap()),
            BoundExpression::Assignment(name, expression) => {
                let expression_result = self.evaluate_expression(expression, variables)?;
                variables.insert(name.name.clone(), expression_result);
                Ok(expression_result)
            }
            BoundExpression::Unary(operator, operand) => {
                let operand_result = self.evaluate_expression(&operand, variables)?;

                match operator.kind {
                    BoundUnaryOperatorKind::Identity => {
                        if let Value::I32(num) = operand_result {
                            Ok(Value::I32(num))
                        } else {
                            Err(EvalError::UnexpectedValueType)
                        }
                    }
                    BoundUnaryOperatorKind::Negation => {
                        if let Value::I32(num) = operand_result {
                            Ok(Value::I32(-num))
                        } else {
                            Err(EvalError::UnexpectedValueType)
                        }
                    }
                    BoundUnaryOperatorKind::LogicalNegation => {
                        if let Value::Bool(boolean) = operand_result {
                            Ok(Value::Bool(!boolean))
                        } else {
                            Err(EvalError::UnexpectedValueType)
                        }
                    }
                }
            }
            BoundExpression::Literal(literal) => match literal {
                BoundLiteralExpression::Integer(integer) => match integer {
                    Integer::I32(num) => Ok(Value::I32(*num)),
                },
                BoundLiteralExpression::Boolean(boolean) => Ok(Value::Bool(*boolean)),
            },
        }
    }
}

#[derive(Debug)]
pub enum EvalError {
    UnsupportedBinaryOperandType,
    UnsupportedUnaryOperandType,
    UnexpectedValueType,
}
